/*
 * MotorDriver.h
 *
 *
 *      Author: squanixmachine
 */

#ifndef MOTORDRIVER_H_
#define MOTORDRIVER_H_

#define Delay_Motor 1000
#define Delay_Turn 1500

void turnright();
void turnleft();
void forward();
void bacrward();
void stops();
void Realstops();

#endif /* MOTORDRIVER_H_ */
